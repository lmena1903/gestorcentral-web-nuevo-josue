class FilteringCategorizationsController < ApplicationController
  before_action :set_filtering_categorization, only: [:show, :edit, :update, :destroy]

  # GET /filtering_categorizations
  # GET /filtering_categorizations.json
  def index
    @filtering_categorizations = FilteringCategorization.all

    @hora = Time.now - 10.days
    @tiempo = @hora.strftime('%Y-%m-%d')
  end

  # GET /filtering_categorizations/1
  # GET /filtering_categorizations/1.json
  def show
    @@id2 = params[:id]

    @id = @@id2

    @hora = Time.now
    @tiempo = @hora.strftime('%Y-%m-%d')


       sql = "select idfiltro as filtro , count(*) as suma from tbitacora where idfiltro in (select Id_filter from categori_filters where id_categori = #{@@id2}) group by idfiltro"

    @datos = ActiveRecord::Base.connection.exec_query(sql)

    hash = {}
    chash = {}

    datosScript = Array.new
    @datos = ActiveRecord::Base.connection.exec_query(sql)

    fil_cat = TxFilters.all

    contador = 0
    fil_cat.each do |fi_ca|
      contador += 1
      hash.store(fi_ca.Id,fi_ca.Description)
      chash.store(fi_ca.Id,Colors.obtenerColor(contador))

    end


    @datos.each do |da|
      idCar = da['filtro']
      valor = da['suma'].to_s.downcase.tr(" ", "_")
      label = hash[idCar].to_s.downcase.tr(" ", "_")
      color = chash[idCar].to_s.downcase.tr(" ", "_")
      registro = "label: '#{label}', data: #{valor}, color: '#{color}'"

      if registro.to_s.length
        datosScript.push(("{#{registro} },").gsub(" ", " ").html_safe).to_s
      end

      #registro = "label: '#{label}', data: #{valor}"

    end

    @valores = datosScript

    # registro = ""
    # hora = ""
    # hora2 = ""
    # count = 0
    # keys = {}
    # color = {}
    # datosScript = Array.new
    # total = 0
    # hash = {}
    # colorHash = {}
    #
    # @tx_filter = TxFilters.where("filtering_categorization_id = #{@@id2}")
    # @tx_filter.each do |txfil|
    #   hash.store(txfil.Id, txfil.Description)
    #   colorHash.store(txfil.Id, txfil.Backgroundcolor)
    # end
    #
    # @datos.each do |datos|
    #   idFil = datos['idfiltro']
    #   hora = datos['hora']
    #   valores = datos['count']
    #
    #   comp = hash[idFil]
    #   label = hash[idFil].to_s.downcase.tr(" ", "_")
    #
    #   if hora != hora2.to_s
    #     if registro.to_s.length > 0
    #       total = count
    #       datosScript.push(("{"+registro+"},").gsub("", "").html_safe).to_s
    #     end
    #     count = 0
    #     hora2 = hora
    #     registro = "time: '"+hora2+"'"
    #   end
    #   count += 1
    #
    #   if total <= count
    #     keys.store(count, label)
    #     color.store(count, colorHash[idFil])
    #   end
    #
    #   registro += ",#{label}: #{valores}"
    # end
    #
    # if registro.to_s.length > 0
    #   datosScript.push(("{#{registro}}").gsub("", "").html_safe).to_s
    # end
    #
    # keysScript = Array.new
    # colorScript = Array.new
    # n = 0
    # keys.each do |k|
    #   nu = n+=1
    #   if nu < keys.length
    #     keysScript.push(("#{k[1]}").gsub("", "").html_safe).to_s
    #   elsif nu = keys
    #     keysScript.push(("#{k[1]}").gsub("", "").html_safe).to_s
    #   end
    # end
    #
    # d = 0
    # color.each do |c|
    #   nu = d+=1
    #   if nu < color.length
    #     colorScript.push(("#{c[1]}").gsub("", "").html_safe).to_s
    #   elsif nu = color
    #     colorScript.push(("#{c[1]}").gsub("", "").html_safe).to_s
    #   end
    # end
    #
    # keysScriptD = Array.new
    # colorScriptD = Array.new
    # n = 0
    # keys.each do |k|
    #   nu = n+=1
    #   if nu < keys.length
    #     keysScriptD.push(("'#{k[1]}'").gsub("", "").html_safe).to_s
    #   elsif nu = keys
    #     keysScriptD.push(("'#{k[1]}'").gsub("", "").html_safe).to_s
    #   end
    # end
    #
    # d = 0
    # color.each do |c|
    #   nu = d+=1
    #   if nu < color.length
    #     colorScriptD.push(("'#{c[1]}'").gsub("", "").html_safe).to_s
    #   elsif nu = color
    #     colorScriptD.push(("'#{c[1]}'").gsub("", "").html_safe).to_s
    #   end
    # end
    #
    # @label = keysScript
    # @color = colorScript
    #
    # @labelD = keysScriptD
    # @colorD = colorScriptD
    # @datosScript = datosScript

  end

  def dynamic

    render :partial => 'filtering_categorizations/dynamic'
  end

  # GET /filtering_categorizations/new
  def new
    @filtering_categorization = FilteringCategorization.new
    @btn_edit = false

    #sql = "select * from tx_filters"
    #@filtros = ActiveRecord::Base::connection.exec_query(sql)
    @filtros = TxFilters.all

    gon.fil = @filtros
    gon.validate = false
  end

  # GET /filtering_categorizations/1/edit
  def edit
    @filtros = TxFilters.all

    @btn_edit = true

    gon.fil = @filtros
    gon.validate = false

  end

  # POST /filtering_categorizations
  # POST /filtering_categorizations.json
  def create
    @filtering_categorization = FilteringCategorization.new(filtering_categorization_params)



    respond_to do |format|
      if @filtering_categorization.save

        if params[:sel_fil].present?

          @valores = params[:sel_fil].split(",")

           count = 0
           val = ""
           @valores.each do |sf|
             @txfiltro = TxFilters.find_by_Id(sf)

             @cat_fil = CategoriFilter.new
             @cat_fil.id_categori = @filtering_categorization.id
             @cat_fil.id_filter = @txfiltro.Id
             @cat_fil.save

           end

           nuevo = val
           puts nuevo

        end

        #format.html { redirect_to @filtering_categorization, notice: 'Filtering categorization was successfully created.' }
        format.html { redirect_to filtering_categorizations_path, notice: 'Filtering categorization was successfully created.' }
        format.json { render :show, status: :created, location: @filtering_categorization }
      else
        format.html { render :new }
        format.json { render json: @filtering_categorization.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /filtering_categorizations/1
  # PATCH/PUT /filtering_categorizations/1.json
  def update
    respond_to do |format|
      if @filtering_categorization.update(filtering_categorization_params)
        #format.html { redirect_to @filtering_categorization, notice: 'Filtering categorization was successfully updated.' }
        format.html { redirect_to filtering_categorizations_path, notice: 'Filtering categorization was successfully updated.' }
        format.json { render :show, status: :ok, location: @filtering_categorization }
      else
        format.html { render :edit }
        format.json { render json: @filtering_categorization.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /filtering_categorizations/1
  # DELETE /filtering_categorizations/1.json
  def destroy
    @filtering_categorization.destroy
    respond_to do |format|
      format.html { redirect_to filtering_categorizations_url, notice: 'Filtering categorization was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_filtering_categorization
    @filtering_categorization = FilteringCategorization.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def filtering_categorization_params
    params.require(:filtering_categorization).permit(:name_categorization)
  end
end