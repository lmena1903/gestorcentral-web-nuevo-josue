class XlsMailer < ApplicationMailer

  default from: "krodriguez@kssoluciones.com.mx"

  def send_xls_es(fecha_comienzo, usuario, file, fecha_termino)
    @inicio = fecha_comienzo
    @use = usuario.name + ' ' + usuario.last_name
    @file = file
    @path_file = 'public/generatedXLS/alerts/'+ @file
    @fin = fecha_termino

    attachments[@file] = File.read("#{@path_file}")
    mail to: usuario.email , subject: "Exportación de Alerta a Excel lista: #{@file.to_s}"
  end

  def send_xls(fecha_comienzo, usuario, file, fecha_termino)
    @inicio = fecha_comienzo
    @use = usuario.name + ' ' + usuario.last_name
    @file = file
    @path_file = 'public/generatedXLS/alerts/'+ @file
    @fin = fecha_termino

    attachments[@file] = File.read("#{@path_file}")
    mail to: usuario.email , subject: "Export ALert to Excel is ready: #{@file.to_s}"
  end

  def send_level_xls_es(fecha_comienzo, usuario, file, fecha_termino, asunto)
    @inicio = fecha_comienzo
    @use = usuario.name + ' ' + usuario.last_name
    @file = file
    @path_file = 'public/generatedXLS/niv_alert/'+ @file
    @fin = fecha_termino
    @asunto = asunto

    attachments[@file] = File.read("#{@path_file}")
    mail to: usuario.email , subject: "#{@asunto} #{@file.to_s}"
  end

  def send_level_xls(fecha_comienzo, usuario, file, fecha_termino, asunto)
    @inicio = fecha_comienzo
    @use = usuario.name + ' ' + usuario.last_name
    @file = file
    @path_file = 'public/generatedXLS/niv_alert/'+ @file
    @fin = fecha_termino
    @asunto = asunto

    attachments[@file] = File.read("#{@path_file}")
    mail to: usuario.email , subject: "#{@asunto} #{@file.to_s}"
  end
end
