class TxFilters < ApplicationRecord
  has_many :tbitacora
  has_many :categori_filters
  has_many :tx_filter_details
  has_many :tx_schedule_references
  has_many :tx_filter_schedules

  #belongs_to :filtering_categorization, foreign_key: :filtering_categorization_id
end