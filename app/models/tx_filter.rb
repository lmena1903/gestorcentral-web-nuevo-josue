class TxFilter < ApplicationRecord
  belongs_to :tx_session
  has_many :tx_filter_details, :foreign_key => "id"
  # has_many :tx_filters, :foreign_key => "Session_id", :dependent => :destroy
end
