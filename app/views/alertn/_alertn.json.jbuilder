json.extract! alertn, :id, :created_at, :updated_at
json.url alertn_url(alertn, format: :json)
