require 'test_helper'

class TxScheduleMessagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tx_schedule_message = tx_schedule_messages(:one)
  end

  test "should get index" do
    get tx_schedule_messages_url
    assert_response :success
  end

  test "should get new" do
    get new_tx_schedule_message_url
    assert_response :success
  end

  test "should create tx_schedule_message" do
    assert_difference('TxScheduleMessage.count') do
      post tx_schedule_messages_url, params: { tx_schedule_message: { Num_Alarma: @tx_schedule_message.Num_Alarma, background_color: @tx_schedule_message.background_color, bit_date: @tx_schedule_message.bit_date, bit_description: @tx_schedule_message.bit_description, bit_escala: @tx_schedule_message.bit_escala, bit_limit: @tx_schedule_message.bit_limit, bit_reference: @tx_schedule_message.bit_reference, letter_color: @tx_schedule_message.letter_color, message: @tx_schedule_message.message, schedule_id: @tx_schedule_message.schedule_id } }
    end

    assert_redirected_to tx_schedule_message_url(TxScheduleMessage.last)
  end

  test "should show tx_schedule_message" do
    get tx_schedule_message_url(@tx_schedule_message)
    assert_response :success
  end

  test "should get edit" do
    get edit_tx_schedule_message_url(@tx_schedule_message)
    assert_response :success
  end

  test "should update tx_schedule_message" do
    patch tx_schedule_message_url(@tx_schedule_message), params: { tx_schedule_message: { Num_Alarma: @tx_schedule_message.Num_Alarma, background_color: @tx_schedule_message.background_color, bit_date: @tx_schedule_message.bit_date, bit_description: @tx_schedule_message.bit_description, bit_escala: @tx_schedule_message.bit_escala, bit_limit: @tx_schedule_message.bit_limit, bit_reference: @tx_schedule_message.bit_reference, letter_color: @tx_schedule_message.letter_color, message: @tx_schedule_message.message, schedule_id: @tx_schedule_message.schedule_id } }
    assert_redirected_to tx_schedule_message_url(@tx_schedule_message)
  end

  test "should destroy tx_schedule_message" do
    assert_difference('TxScheduleMessage.count', -1) do
      delete tx_schedule_message_url(@tx_schedule_message)
    end

    assert_redirected_to tx_schedule_messages_url
  end
end
