//---Traducciones Inputs
let placeAddList = $('#traduPlaceAddList').val();
let sorrySwal = $('#swalSorry').val();
let traduCondiIncompleta = $('#swalCondiIncompleta').val();
let traduCondiExist = $('#swalCondiExiste').val();
let traduCondiDelete = $('#swalCondiDelete').val();
let titleCanel = $('#titleCancel').val();
let txtCancel = $('#txtCancel').val();
let txtAnd = $('#txtAnd').val();
let txtOr = $('#txtOr').val();
let swalDelete = $('#swalDelete').val();
let dataWillLose = $('#dataWillLose').val();
let yourSure = $('#youSure').val();
let btn_yes = $('#btn_yes').val();
let btn_confir = $('#btn_confir').val();
let btn_cancel = $('#btn_cancel').val();
let btn_cancel2 = $('#btn_cancel2').val();
let noPalabra = $('#noPalabra').val();
let noAddCone = $('#noAddCone').val();
let noAddSinCon = $('#noAddSinCon').val();
let msnBtnDelete = $('#deleteSession').val(),
    enableEdition = $('#enableEdition').val(),
    attention = $('#attention').val(),
    save_change = $('#save_change').val();


//---Radio Button i-checks
$('.i-checks').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
});

//---Cambia el valor del input Denied
$('.icheckbox_square-green').on('ifClicked', function(){
    let $_this = $('.icheckbox_square-green').hasClass('checked');

    if ( !$_this ){

        $('#denied').val('1');

    } else {

        $('#denied').val('0');

    }
});

//Al entrar en la pagina
$(document).ready(function(){

    //Time Picker
    $('.clockpicker').clockpicker();

    //Tipo de filtro
    let tipoFiltro = $('#tipoFiltro').val();
    $('#typeFilter').val(`${tipoFiltro}`);


    //-- Id de la sesión a la cual se le agregara el nuevo filtro
    idSession = $('#filterSession').val();
    console.log(idSession);

    for (let i = 0; i < gon.sesiones.length; i++){
        let sesionId = gon.sesiones[i].Id
        let layId = gon.sesiones[i].Lay_id
        let formatId = gon.sesiones[i].Format_id

        if ( idSession == sesionId ){
            // alert('Sessiones iguales');


            for ( let c = 0; c < gon.tCamposFormatos.length; c++ ){

                if ( (gon.tCamposFormatos[c].IdLay == layId ) && ( gon.tCamposFormatos[c].IdFormato == formatId) ){

                    for( let d = 0; d < gon.tCamposLays.length; d++ ){

                        if ( ( gon.tCamposLays[d].IdLay == gon.tCamposFormatos[c].IdLay ) && ( gon.tCamposLays[d].IdCampo == gon.tCamposFormatos[c].IdCampo )  ){

                            if ( gon.tCamposLays[d].Alias != ""){

                                let trCampo = `<tr>
                                                <td><input type="checkbox" value="${gon.tCamposLays[d].IdCampo}" class="check-id-Campo" disabled ></td>
                                                <td>${gon.tCamposLays[d].Alias}</td>
                                               </tr>`;

                                $("#selectFeild").append(`<option value="${gon.tCamposLays[d].IdCampo}" name="select_feild">${gon.tCamposLays[d].Alias}</option>`);
                                $("#contentCamposRef").append(trCampo);

                            } else {

                                let trCampo = `<tr>
                                        <td><input type="checkbox" value="${gon.tCamposLays[d].IdCampo}" class="check-id-Campo" disabled ></td>
                                        <td>${gon.tCamposLays[d].Nombre}</td>
                                       </tr>`;

                                $("#selectFeild").append(`<option value="${gon.tCamposLays[d].IdCampo}" name="select_feild">${gon.tCamposLays[d].Nombre}</option>`);
                                $("#contentCamposRef").append(trCampo);
                            }
                        }
                    }
                }
            }
        }
    }


    //Agrecgar check a los inputs de campos referencia
    let idFiltro = $('#idFiltro').val(),
        inputs = $("#contentCamposRef").find('input');

    // Cilco for donde se agrega el checked a los input
    for( let i = 0; i < gon.camposRef.length; i++ ){
        // alert('Entre ciclo checks')
        let Filtro_id = gon.camposRef[i].Filter_id,
            idcampo = gon.camposRef[i].field_formatId;

        if ( idFiltro == Filtro_id){

            inputs.each(function(){
                let valIdCampocheck = $(this).val()

                if ( idcampo == valIdCampocheck ){
                    $(this).prop('checked', true);
                }

            });
        }
    }

    // let tipoLimiteTran = $('#tipoLimiteTran').val(),
    //     selUMinterval = $('#selUMinterval').val(),
    //     selUMIntermediate = $('#selUMIntermediate').val(),
    //     selUMTimeout = $('#selUMTimeout').val();
    //
    // $('#limite').val(`${tipoLimiteTran}`);
    // $('#select_Interval_time').val(`${selUMinterval}`);
    // $('#select_intermediate_time').val(`${selUMIntermediate}`);
    // $('#select_timeout_time').val(`${selUMTimeout}`)

});

//---Crea la paleta de colores con jquery-minicolors-rails
$('.demo').each(function () {
    $(this).minicolors({
        control: $(this).attr('data-control') || 'hue',
        defaultValue: $(this).attr('data-defaultValue') || '',
        format: $(this).attr('data-format') || 'hex',
        keywords: $(this).attr('data-keywords') || '',
        inline: $(this).attr('data-inline') === 'true',
        letterCase: $(this).attr('data-letterCase') || 'lowercase',
        opacity: $(this).attr('data-opacity'),
        position: $(this).attr('data-position') || 'bottom left',
        swatches: $(this).attr('data-swatches') ? $(this).attr('data-swatches').split('|') : [],
        change: function (value, opacity) {
            if (!value) return;
            if (opacity) value += ', ' + opacity;
            if (typeof console === 'object') {
                //console.log(value);
            }
        },
        theme: 'bootstrap'
    });
});

//Si el usuario quiere cambiar de filtro
let filtroEdit = $('#tipoFiltro').val();
$('#typeFilter').on('change', function(){

    let option = $('#typeFilter option:selected').text();
    let cambiofiltro = this.value;

    if ( filtroEdit == 0 && (cambiofiltro == 1 || cambiofiltro == 2 )) {
        swal({
            title: "Atención",
            text: `Estas cambiando un filtro Principal a ${option}, esto afectara el historico.`,
            type: "warning",
            timer: 5000,
        });

        if ( cambiofiltro == 1 ){
            $('.conte-color-filter').css('display', 'block');
        } else {
            $('.conte-color-filter').css('display', 'none');
            $('#tx_filters_Backgroundcolor').val();
        }
    } else {
        $('.conte-color-filter').css('display', 'none');
        $('#tx_filters_Backgroundcolor').val();
    }

});

//Editar Filtro
$('#active_filter').on('click', function(){
   // alert('Entre click update')

    let $_this = $(this);

    if ( $_this.val() == '1' ){
        $_this.val(`0`);
        $('#desFilter, #colorFilter, #typeFilter').attr('disabled', false);
    } else if ( $_this.val() == '0' ) {
        $_this.val(`1`);
        $('#desFilter, #colorFilter, #typeFilter').attr('disabled', true);
    }
});


//######## SCRIPTS PARA EL AGREGAR CONDICIONES AL FILTRO - UPLOAD

//Editar Detalle del Filtro
$('#active_detail_filter').on('click', function(){
    // alert('Entre click update')

    let $_this = $(this);

    if ( $_this.val() == '1' ){
        $_this.val(`0`);
        $('#selectFeild, #operator_sel, #valor, #valOneBetween, #valTwoBetween, #selectList, #selectConnector, #selectParentesis').attr('disabled', false);
        $('.contDenied, .addCondi, .eliminar').fadeIn(300);
    } else if ( $_this.val() == '0' ) {
        $_this.val(`1`);
        $('#selectFeild, #operator_sel, #valor, #valOneBetween, #valTwoBetween, #selectList, #selectConnector, #selectParentesis').attr('disabled', true);
        $('.contDenied, .addCondi, .eliminar').fadeOut(300);
    }
});

//---Validaciones para mostar (VALUE) || (BETWEEN / NOT BETWEEN) || (IN / NOT)
$('.operator_sel').change(function(){

    $(".betweenValue label").remove();
    $(".inNot label").remove();
    let operadorText = $('.operator_sel option:selected').text();
    let operadorVal = $('.operator_sel').val();

    //---Validaciones para mostar (VALUE) || (BETWEEN / NOT BETWEEN) || (IN / NOT)
    if ( operadorVal === "8" ){
        $('.betweenValue').prepend(`<label style="display: block">${operadorText}</label>`);

        $('.oneValue, .inValue, .contDenied').css('display','none');
        $('.betweenValue').css('display','block');
        $('#valor, #selectList').val("");

        //---Reset CheckBox Denied
        $('#denied').val('0');
        $('.icheckbox_square-green').removeClass('checked');

    } else if( operadorVal === "7" ){
        $('.inNot').prepend(`<label style="display: block">${operadorText}</label>`);

        $('.oneValue, .betweenValue, .contDenied').css('display','none');
        $('.inValue').css('display','flex');
        $('#valor, .valOneBetween, .valTwoBetween').val("");

        //---Reset CheckBox Denied
        $('#denied').val('0');
        $('.icheckbox_square-green').removeClass('checked');

    } else {

        $('.contDenied, .oneValue').css('display', 'block');
        $('.betweenValue, .inValue').css('display','none');
        $('.valOneBetween, .valTwoBetween, #selectList').val("");

    }

});

//---Script para el boton de carga archivos falso
var btnReal = document.getElementById('realBtnFile');

$('.falseBtnFile').on('click', function(){
    btnReal.click();
});

$('.realBtnFile').change(function(){
    var valFile = $(this).val()

    if ( valFile ){
        let txtFile = valFile.match( /[\/\\]([\w\d\s\.\-\(\)]+)$/)[1];
        $('.customText').html(txtFile);
        $('.customText').attr('value', txtFile);
        $('#selectList').fadeOut(100);
    } else {
        $('.customText').html(`${placeAddList}`);
        $('.customText').attr('value', '');
        $('#selectList').fadeIn(100);
    }
});

// Bloqueo select In
$('#selectList').on('change', function(){
    // alert('Entre list')
    let $_this = $(this).val();

    if( $_this !== "" ){
        $('.upFiles').fadeOut(100);
    } else {
        $('.upFiles').fadeIn(100);
    }

});

let arrayOneValue = [];

let arrayBetween = [];

let arrayList = [];

//---Número de Clase
let numCon = 0;

//---Efecto fade
let fadeIn = 1000;

// Enumera todas las condiciones
function listarCondiciones(){
    let num = 0;
    let tr = $('tbody').find('tr');

    for (let i = 0; i < tr.length ; i++){
        num++;
        let trs = tr[i];

        let tdNum = $(trs).children('td.num_condi')
        tdNum.html(`${num}`);
    }

}

function condicionIncompleta(){
    swal({
        title: `${sorrySwal}`,
        text: `${traduCondiIncompleta}`,
        type: "error"
    });
}

function condiExiste(){
    swal({
        title: `${sorrySwal}`,
        text: `${traduCondiExist}`,
        type: "error"
    });
}


//-- Agregar condiciones
$('.addConditions').on('click', function(){

    let conditionText;
    let denegadoBetweenList;

    //-- Texto de los Select
    let campoText = $('.selectFeild option:selected').text();
    let operadorText = $('.operator_sel option:selected').text();
    let listText = $('#selectList option:selected').text();
    let listUpload = $('#customText').text();
    let connector =  $('#selectConnector option:selected').text();

    //-- Valores de los Select
    let campoVal = $('.selectFeild').val();
    let operadorVal = $('.operator_sel').val();
    let conectorVal = $('#selectConnector').val();

    //-- Valores de Listas
    let selectListVal = $('#selectList').val();
    let fileUpload = $('#realBtnFile').val();

    //-- Valores de los Inputs
    let unValor = $('#valor').val();
    let valOneBetween = $('.valOneBetween').val();
    let valTwoBetween = $('.valTwoBetween').val();

    //-- Check de Negado
    let denied = $('#denied').val();

    //---Agrega la palabra "NO" - menos Between / Not Between - In / Not In
    if( denied === "1"){

        negado = `${noPalabra}`;
        // tdDenegado = `<td class="text-center denegado"><input type="checkbox" title="Cambiar denegado" class="changeDenied" checked></td>`;
        tdDenegado = `<td class="text-center num_condi filterCondi"></td>`;

    } else if ( denied === "0" ) {

        negado = "";
        // tdDenegado = `<td class="text-center denegado"><input type="checkbox" title="Cambiar denegado" class="changeDenied"></td>`;
        tdDenegado = `<td class="text-center num_condi filterCondi"></td>`;

    }

    // if(connector === "Select Connector"){
    //     conectorVal = -1;
    //     connector = "-";
    // }

    //---GON TABLAS
    let campos = gon.tCamposLays;



    //-- Guardo la posision y longitud del campo selecionado en una td
    for( let i = 0; i < campos.length; i++ ){

        let campo = campos[i];

        if( campo.IdCampo == campoVal ){

            // alert(`El campo: ${campoVal} coincide con campo: ${campo.IdCampo}`);

            td = `<td class="tdDispNone filterCondi" value="${campo.Posicion}" name="posision">${campo.Posicion}</td>
                      <td class="tdDispNone filterCondi" value="${campo.Longitud}" name="longitud">${campo.Longitud}</td>`;
        }

    }

    let numDeCondi = $('.tBodyCondition').children().length;
    if( numDeCondi === 0 ){
        conectorVal = -1;
        // connector = "";
        tdConector = `<td class="text-center connector"> </td>`;
    } else {
        tdConector = `<td class="text-center connector"> <span class="btn btn-primary btn-xs changeConector" title="Cambiar conector">${connector}</span></td>`;
    }

    tdEliminar = `<td class="text-center"><span class="btn btn-danger btn-xs eliminar" type="button" title="Delete"><i class="fa fa-times"></i></span></td>`;

    if ( "" !== unValor) {

        numCon++;

        //---Creo una variable donde concateno todos los valores
        let conditionText = `${campoText} ${operadorText} ${unValor}`;


        if( campoVal !== ""){
            //value="${conditionText}" name="${numCon}" title="${operadorText}"
            tr = `<tr id="condicion${numCon}" class="condicion" value="${conditionText}" name="${operadorText}"> 
                      ${tdDenegado}
                      ${tdConector}
                      <td class="text-center expresion"><strong class="negado">${negado}</strong> <span class="campo" value="${campoVal}">${campoText}</span> <strong class="operador" value="${operadorVal}">${operadorText}</strong> <span class="valorUno" value="${unValor}">${unValor}</span> </td>
                      ${tdEliminar}
                      <td class="text-center"></td>
                      <td class="tdDispNone filterCondi conectorVal" value="${conectorVal}" name="conector">${conectorVal}</td>
                      <td class="tdDispNone filterCondi denegadoVal" value="${denied}" name="denegado">${denied}</td>
                      <td class="tdDispNone filterCondi" value="${operadorVal}" name="operador">${operadorVal}</td>
                      <td class="tdDispNone filterCondi" value="0" name="parantesis">0</td>
                      <td class="tdDispNone filterCondi" value="${unValor}" name="valor">${unValor}</td>
                      <td class="tdDispNone filterCondi" value="${campoVal}" name="campo">${campoVal}</td>
                      ${td}
                  </tr>`;
        }

        function agregarCondiOneVal(){

            if( ( unValor !== "" && campoVal !== "" && operadorVal !== "" && conectorVal !== "" ) && ( unValor !== "" && campoVal !== "" && operadorVal !== "" )){

                let appCondition = $('.tBodyCondition').append($(tr).fadeIn(fadeIn));

                $('.s_Condition').scrollTop( appCondition.offset().top );

                listarCondiciones()

                arrayOneValue.push(`${conditionText}`);

                console.log(arrayOneValue);

                //--Habilita select de los conectores
                $('#selectConnector').prop('disabled', false);

            } else {
                condicionIncompleta();
            }

        }

        if ( arrayOneValue.length !== 0 ){

            let isDuplicateOne = false;

            //---Recorro todos los valores almacenados en el arreglo para checar si ya existe la condición
            for ( let i = 0; i < arrayOneValue.length; i++ ){

                //---- Esta variable itera todos los valores del arreglo
                let conditionArr = arrayOneValue[i];

                //---Camparo valores obtenidos de los inputs con los valores que ya existen en el arreglo
                if ( conditionText === conditionArr ){
                    numCon--;

                    condiExiste();

                    isDuplicateOne = true;

                }
            }

            //---Si isDuplicate es diferente de "false" se ejecuta el código de adentro
            if ( !isDuplicateOne ){
                agregarCondiOneVal()
            }

        } else{
            agregarCondiOneVal()
        }

        //---Se limpian valores
        $('.selectFeild').val("");
        $('.operator_sel').val("");
        $('#selectConnector').val("");
        $('#valor').val("");
        $('#denied').val('0');
        $('.icheckbox_square-green').removeClass('checked');

        ///-----Valido que los inputs de Between no esten vacios
    }
    else if ( valOneBetween !== "" && valTwoBetween !== "" ){

        numCon++;

        //---Creo Una variable donde concateno los valores Obtenidos de los inputs Between
        conditionText = `${campoText} Between ${valOneBetween} AND ${valTwoBetween}`;

        if( operadorText === " Between "){

            denegadoBetweenList = 0;
            negadoBetIn = "";
            tdDenegado = `<td class="text-center num_condi filterCondi"></td>`;

        } else {

            denegadoBetweenList = 1;
            negadoBetIn = `${noPalabra}`;
            tdDenegado = `<td class="text-center num_condi filterCondi"></td>`;
        }

        //---Varibales para construir la Fila
        // let parAbre = `<strong class="parAbre" value="1">${parentesis}</strong>`;
        // let parCierre = `<strong class="parCierra" value="2">)</strong>`;
        let negadoCampo = `<strong class="negado">${negadoBetIn}</strong>`;
        let campo = `<span class="campo" value="${campoVal}">${campoText}</span>`;
        let operador = `<strong class="operador">Between</strong>`;
        // let menorQue = '<strong class="operador"><=</strong>';
        let valorUno = `<span class="valorUno" value="${valOneBetween}">${valOneBetween}</span>`;
        let valorDos = `<span class="valorDos" value="${valTwoBetween}">${valTwoBetween}</span>`;
        // let conectorAnd = '<strong class="conector">AND</strong>';

        if( campoVal !== ""){
            tr = `<tr id="condicion${numCon}" class="condicion" value="${conditionText}" name="${operadorText}">
                     ${tdDenegado}
                     ${tdConector}
                     <td class="text-center expresion">${negadoCampo} ${campo} ${operador} ${valorUno} <strong class="conector">${txtAnd}</strong> ${valorDos}</td>
                     ${tdEliminar}
                     <td class="text-center"></td>
                     <td class="tdDispNone filterCondi conectorVal" value="${conectorVal}" name="conector">${conectorVal}</td>
                     <td class="tdDispNone filterCondi denegadoVal" value="${denegadoBetweenList}" name="denegado">${denegadoBetweenList}</td>
                     <td class="tdDispNone filterCondi" value="${operadorVal}" name="operador">${operadorVal}</td>
                     <td class="tdDispNone filterCondi" value="0" name="parantesis">0</td>
                     <td class="tdDispNone filterCondi" value="{${valOneBetween}}{${valTwoBetween}}" name="valor">{${valOneBetween}}{${valTwoBetween}}</td>
                     <td class="tdDispNone filterCondi" value="${campoVal}" name="campo">${campoVal}</td>
                     ${td}
                  </tr>
                  `;
        }


        function agregarCondiBetween(){

            if( (campoVal !== "" && operadorVal !== "" && conectorVal !== "" ) && ( campoVal !== "" && operadorVal !== "" )){
                //---Imprime Fila
                $('.tBodyCondition').append($(tr).fadeIn(fadeIn));

                listarCondiciones();

                //--Habilita select de los conectores
                $('#selectConnector').prop('disabled', false);

                arrayBetween.push(`${conditionText}`);

                console.log(arrayBetween);

            } else {
                condicionIncompleta();
            }

        }
        //---Si hay elementos en el arreglo se ejecuta las sig. lineas
        if ( arrayBetween !== 0){

            let isDuplicateTwo = false;

            //---Recorro todos los valores almacenados en el arreglo Between
            for ( let i = 0; i < arrayBetween.length; i++ ){

                //---- Esta variable itera todos los valores del arreglo
                let conditionArr = arrayBetween[i];
                console.log(conditionArr);

                //---Si los valores obetenidos de Feild, Operatos e Inputs Between ya existen en el Arreglo se ejecuta las sig. lineas
                if ( conditionText == conditionArr ){
                    numCon--;

                    condiExiste();

                    isDuplicateTwo = true;

                }
            }

            //---Si isDuplicate es diferente de "false" se ejecuta el código de adentro
            if ( !isDuplicateTwo ){
                agregarCondiBetween()
            }
        }
        else{
            agregarCondiBetween()
        }



        //---Se limpian los valores
        $('.selectFeild').val("");
        $('.operator_sel').val("");
        $('#selectConnector').val("");
        $('.valOneBetween').val("");
        $('.valTwoBetween').val("");
        $('#denied').val('0');
        $('.icheckbox_square-green').removeClass('checked');
    }
    else if ( selectListVal !== "" || fileUpload !== "" ) {

        numCon++;

        if ( selectListVal !== "" ){

            var lista = `${listText}`,
                listVal = $('#selectList').val();

            //---Creo una variable donde concateno todos los valores
            conditionText = `${campoText} In ${listText}`;

        } else {

            var lista = `${listUpload}`,
                listVal = `${listUpload}`;

            //---Creo una variable donde concateno todos los valores
            conditionText = `${campoText} In ${listUpload}`;
        }

        if ( operadorText === " In "){

            denegadoBetweenList = 0;
            negadoBetIn = "";
            tdDenegado = `<td class="text-center num_condi filterCondi"></td>`;

        } else {

            denegadoBetweenList = 1;
            negadoBetIn = "No";
            tdDenegado = `<td class="text-center num_condi filterCondi"></td>`;

        }

        if( campoVal !== ""){
            tr = `<tr id="condicion${numCon}" class="condicion" value="${conditionText}" name="${operadorText}">
                      ${tdDenegado}
                      ${tdConector}
                      <td class="text-center expresion"><strong class="negado">${negadoBetIn}</strong> <span class="campo" value="${campoVal}">${campoText}</span> <strong class="operador" value="${operadorVal}">In</strong> <span class="lista" value="${listVal}"> ${lista} </span> </td>
                      ${tdEliminar}
                      <td class="text-center"></td>
                      <td class="tdDispNone filterCondi conectorVal" value="${conectorVal}" name="conector">${conectorVal}</td>
                      <td class="tdDispNone filterCondi denegadoVal" value="${denegadoBetweenList}" name="denegado">${denegadoBetweenList}</td>
                      <td class="tdDispNone filterCondi" value="${operadorVal}" name="operador">${operadorVal}</td>
                      <td class="tdDispNone filterCondi" value="0" name="parantesis">0</td>
                      <td class="tdDispNone filterCondi" value="${lista}" name="valor">${lista}</td>
                      <td class="tdDispNone filterCondi" value="${campoVal}" name="campo">${campoVal}</td>
                      ${td}
                    </tr>`;
        }


        function agregarCondiList(){

            if( ( campoVal !== "" && operadorVal !== "" && conectorVal !== "" ) && ( campoVal !== "" && operadorVal !== "" ) ){

                let appCondition = $('.tBodyCondition').append($(tr).fadeIn(fadeIn));

                $('.s_Condition').scrollTop( appCondition.offset().top );

                listarCondiciones()

                //--Habilita select de los conectores
                $('#selectConnector').prop('disabled', false);

                arrayList.push(`${conditionText}`);

                console.log(arrayList);

            } else {
                condicionIncompleta();
            }
        }

        if ( arrayList.length !== 0 ){

            var isDuplicateOne = false;

            //---Recorro todos los valores almacenados en el arreglo para checar si ya existe la condición
            for ( let i = 0; i < arrayList.length; i++ ){

                //---- Esta variable itera todos los valores del arreglo
                var conditionArr = arrayList[i];
                console.log(conditionArr);

                //---Camparo valores obtenidos de los inputs con los valores que ya existen en el arreglo
                if ( conditionText === conditionArr ){
                    numCon--;

                    condiExiste();

                    isDuplicateOne = true;

                }
            }

            //---Si isDuplicate es diferente de "false" se ejecuta el código de adentro
            if ( !isDuplicateOne ){
                agregarCondiList()
            }

        } else{
            agregarCondiList()
        }

        $('.upFiles').fadeIn(100);
        $('.selectFeild').val("");
        $('.operator_sel').val("");
        $('#selectConnector').val("");
        $('#selectList').val("").fadeIn(100);
        $('#realBtnFile').val("");
        $('.customText').html(`${placeAddList}`);
        $('#denied').val('0');
        $('.icheckbox_square-green').removeClass('checked');

    } else {
        condicionIncompleta()
    }

});

//-- Eliminar Condiciones del arreglo
function eliminarCondicion(elemento, arrayConditions ){
    // alert("Entre Funcion borrar arreglo");
    // console.log(elemento);
    // console.log(arrayBetween);

    for ( let k in arrayConditions ){
        let condicion = arrayConditions[k];

        if ( elemento === condicion){
            // alert(` Estos ${elemento} es igual a esto ${condicion}`);

            let indice = arrayConditions.indexOf(condicion);

            arrayConditions.splice(indice, 1);

            console.log(arrayConditions);

            swal({
                title: "OK!",
                text: `${traduCondiDelete}`,
                type: "success"
            });

            let numCondi = $('.tBodyCondition').children().length;

            if( numCondi === 0 ){
                $('#selectConnector').prop('disabled', true);
            }

        }
    }

}

//-- Cambiar tipo de conector
$('.tBodyCondition').on('click', 'span.changeConector', function(){
    // alert("entre Conector");

    let tdPadre = $(this).parent();
    let tdConector = tdPadre.siblings('td.conectorVal');

    if ( tdConector.html() === "0" ){

        tdConector.html('1');
        $(this).html(`${txtAnd}`);

    } else {
        tdConector.html('0');
        $(this).html(`${txtOr}`);
    }

});

let colors = ['rgb(246, 229, 141,.5)', 'rgb(255, 190, 118,.5)', 'rgb(186, 220, 88,.5)', 'rgb(223, 249, 251,.5)', 'rgb(253, 121, 168,.5)', 'rgb(225, 112, 85,.5)'],
    indexColor = 0;

//-- Agregar bloque de Parenthesis
$('#selectParentesis').on('change', function(){
    // alert("Entre Parentesis");

    let longCondi = $('.tBodyCondition').children().length;
    let valorConector = $(this).val();
    let valNegado = $('#selectParentesis option:selected').attr('name')
    let conectorTxt = $('#selectParentesis option:selected').attr('title');
    let parText = $('#selectParentesis option:selected').text();


    //---Agrega la palabra "NO" - menos Between / Not Between - In / Not In
    if( valNegado === "1" ){

        negadoPar = `${noPalabra}`;
        tdDenegadoPar = `<td class="text-center num_condi filterCondi"></td>`;

    } else {

        negadoPar = "";
        tdDenegadoPar = `<td class="text-center num_condi filterCondi"></td>`;

    }


    numCon++;

    if( longCondi !== 0 ){
        spanConector = `<span class="btn btn-primary btn-xs changeConector" title="Cambiar conector">${conectorTxt}</span>`;
    } else {
        spanConector = '';
    }

    let abrePartr = `<tr id="condicion${numCon}" class="parAbre" style="background-color: ${colors[indexColor]};">
                  ${tdDenegadoPar}
                  <td class="text-center connector"> ${spanConector}</td>
                  <td class="text-center expresion"><strong class="negado">${negadoPar}</strong> <span class="parenthesis">(</span></td>
                  <td class="text-center"><span class="btn btn-danger btn-xs eliminar" type="button" title="Delete"><i class="fa fa-times"></i></span></td>
                  <td class="text-center"></td>
                  <td class="tdDispNone filterCondi conectorVal" value="${valorConector}" name="conector">${valorConector}</td>
                  <td class="tdDispNone filterCondi denegadoVal" value="${valNegado}" name="denegado">${valNegado}</td>
                  <td class="tdDispNone filterCondi" value="0" name="operador">-1</td>
                  <td class="tdDispNone filterCondi" value="1" name="parantesis">1</td>
                  <td class="tdDispNone filterCondi" value="0" name="valor"></td>
                  <td class="tdDispNone filterCondi" value="0" name="campo">0</td>
                  <td class="tdDispNone filterCondi" value="0" name="posision">0</td>
                  <td class="tdDispNone filterCondi" value="0" name="longitud">0</td>
              </tr>`;


    let cierrePartr = `<tr class="condicion${numCon} parCierre" style="background-color: ${colors[indexColor]};">
                              <td class="text-center num_condi filterCondi"></td>
                              <td class="text-center connector"></td>
                              <td class="text-center expresion parCierre"><strong class="parenthesis">)</strong></td>
                              <td class="text-center"><span class="btn btn-danger btn-xs eliminar" type="button" title="Delete"><i class="fa fa-times"></i></span></td>
                              <td class="text-center"></td>
                              <td class="tdDispNone filterCondi conectorVal" value="-1" name="conector">-1</td>
                              <td class="tdDispNone filterCondi denegadoVal" value="0" name="denegado">0</td>
                              <td class="tdDispNone filterCondi" value="0" name="operador">-1</td>
                              <td class="tdDispNone filterCondi" value="2" name="parantesis">2</td>
                              <td class="tdDispNone filterCondi" value="0" name="valor"></td>
                              <td class="tdDispNone filterCondi" value="0" name="campo">0</td>
                              <td class="tdDispNone filterCondi" value="0" name="posision">0</td>
                              <td class="tdDispNone filterCondi" value="0" name="longitud">0</td>
                          </tr>`;

    if( longCondi !== 0 ) {
        // alert("Entre mayor");

        if( valorConector === "-1" ){
            swal({
                title: "ERROR!",
                text: `${noAddSinCon}`,
                type: "error"
            });

            numCon--;
            indexColor--;

        } else {

            $('.tBodyCondition').append($(abrePartr).fadeIn(fadeIn) );
            let appCondition = $('.tBodyCondition').append($(cierrePartr).fadeIn(fadeIn) );

            $('.s_Condition').scrollTop( appCondition.offset().top );
            ordenFiltro();
            listarCondiciones();

        }

    } else {

        if( valorConector === "0" || valorConector === "1"){

            swal({
                title: "ERROR!",
                text: `${noAddCone}`,
                type: "error"
            });

            numCon--;
            indexColor--;

        } else {

            $('.tBodyCondition').append($(abrePartr).fadeIn(fadeIn) );
            let appCondition = $('.tBodyCondition').append($(cierrePartr).fadeIn(fadeIn) );

            $('.s_Condition').scrollTop( appCondition.offset().top );
            ordenFiltro();
            listarCondiciones();

            //--Habilita select de los conectores
            $('#selectConnector').prop('disabled', false);
        }
    }

    $(this).val("");

    indexColor++;
    if( indexColor === 6){

        indexColor = 0;
    }

});


//Drag & Dop filter
let left = $('#drag-elements');

var drake =  dragula([left[0]],{
    revertOnSpill: true
});

function ordenFiltro(){
    let allCondi = $('#drag-elements').children(); // Todas las condiciones
    let btnAnd = `<span class="btn btn-primary btn-xs changeConector">${txtAnd}</span>`;

    $('#drag-elements tr.condicion').each(function(){
        let allIndex_Condiciones = $(this).index();  // Index de todas las tr con clase "condicion"
        // alert(allIndex_Condiciones);
        let indexConArriba = allIndex_Condiciones - 1;
        let queHayArriba = allCondi.eq(indexConArriba); // Selecionando en elementos que esta de bajo de parAbre
        // console.log(queHayArriba);

        if ( allIndex_Condiciones !== 0){

            if( queHayArriba.hasClass('condicion') || queHayArriba.hasClass('parCierre') ){

                if( $(this).children('td.conectorVal').html() === "-1"  ){
                    $(this).children('td.connector').html(btnAnd);
                    $(this).children('td.conectorVal').html('1');
                }

            } else if( queHayArriba.hasClass('parAbre') ){

                if( $(this).hasClass('condicion') ){
                    $(this).children('td.connector').html('');
                    $(this).children('td.conectorVal').html('-1');
                }
            }

        } else {
            $(this).children('td.connector').html('');
            $(this).children('td.conectorVal').html('-1');
        }

    });

    $('#drag-elements tr.parAbre').each(function(){
        let allIndex_Parentesis = $(this).index();
        let indexConArriba = allIndex_Parentesis- 1;
        let queHayArriba = allCondi.eq(indexConArriba);

        if( allIndex_Parentesis === 0 || queHayArriba.hasClass('parAbre')){

            $(this).children('td.connector').html('');
            $(this).children('td.conectorVal').html('-1');

        } else {

            if($(this).children('td.conectorVal').html() === '-1'){
                $(this).children('td.connector').html(btnAnd);
                $(this).children('td.conectorVal').html('1');
            }

        }

    });
};

// Handle Events
drake.on('drop', function(el, target, source, sibling){

    let actualizar = $('#active_detail_filter').val();

    if (actualizar == '1'){
        //Cancela el movimiento relizado del drop por el usuario
        drake.cancel(true);

        swal({
            title: `${sorrySwal}`,
            text: `${enableEdition}`,
            type: "error"
        });

    } else {
        ordenFiltro();
        listarCondiciones();
    }
});


let condicionesFiltro = [];
//Actualizar Filtro
let form = document.getElementById('formActualizar');
$('.upload').on('click', function(){

    let valFiltro = $('#active_filter').val(),
        valDetalleFiltro = $('#active_detail_filter').val();

    if ( valFiltro == "0" || valDetalleFiltro == "0" ){
        let td = $('.tBodyCondition').find('td.filterCondi');

        td.each(function () {
            valor = $(this).html();
            condicionesFiltro.push(valor);

            $('#filtroNew').val(condicionesFiltro)
        });

        form.submit();
    } else {
        swal({
            title: `${attention}`,
            text: `${save_change}`,
            type: "error"
        });
    }

});



//###############################################################################################################################################################################
//###############################################################################################################################################################################
//###############################################################################################################################################################################
// Scripts para Metodo Edit

$(document).ready(function(){
    let valFiltro = $('#tipoFiltro').val();
    $('#typeFilter').val(`${valFiltro}`);

    let campoVal = $('#campoId').val(), // Valor del campo
        posVal = $('#posEdit').val(), // Valor de la posision
        lengVal = $('#lengEdit').val(), // Valor de la longitud


        operatorVal = $('#operatorVal').val(); // Valor del operador


    // -- Id de la sesión a la cual se le agregara el nuevo filtro
    idSession = $('#idSession').val();
    console.log(idSession);

    for (let i = 0; i < gon.sesiones.length; i++){
        let sesionId = gon.sesiones[i].Id
        let layId = gon.sesiones[i].Lay_id
        let formatId = gon.sesiones[i].Format_id

        if ( idSession == sesionId ){
            // alert('Sessiones iguales');


            for ( let c = 0; c < gon.tCamposFormatos.length; c++ ){

                if ( (gon.tCamposFormatos[c].IdLay == layId ) && ( gon.tCamposFormatos[c].IdFormato == formatId) ){

                    for( let d = 0; d < gon.tCamposLays.length; d++ ){

                        if ( ( gon.tCamposLays[d].IdLay == gon.tCamposFormatos[c].IdLay ) && ( gon.tCamposLays[d].IdCampo == gon.tCamposFormatos[c].IdCampo )  ){

                            if ( gon.tCamposLays[d].Alias != ""){

                                $("#selectCampo").append(`<option value="${gon.tCamposLays[d].IdCampo}" name="select_feild">${gon.tCamposLays[d].Alias}</option>`);

                            } else{
                                $("#selectCampo").append(`<option value="${gon.tCamposLays[d].IdCampo}" name="select_feild">${gon.tCamposLays[d].Nombre}</option>`);
                            }

                        }
                    }
                }
            }
        }
    }


    $('#selectCampo').val(`${campoVal}`);
    $('#selectOperator').val(`${operatorVal}`);

});


// $('.eliminar').on('click', function(){
//     // alert('entre alert');
//
//     $(this).parents('tr').remove();
//
//     listarCondiciones();
//
//     ordenFiltro();
// });


function totalCondi(){
    let totalCondi = $('.tBodyCondition').children().length;
    $('#totalCondi').val(`${totalCondi}`);
    // alert(`${totalCondi}`)
};

$(document).ready(function(){
   totalCondi();
});

//Eliminar condiciones
$('.tBodyCondition').on('click', '.eliminar', function(){

    let tr = $(this).parents('tr');
    // $.ajax({
    //     type: "POST",
    //     beforeSend: function (xhr) {
    //         xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
    //     },
    //     url: "/tx_filter_details/" + idCondi,
    //     data: {"_method": "delete"},
    // });
    tr.remove();
    listarCondiciones();
    ordenFiltro();
});

$('.conectorEdit').on('click', function(){
    let conector = $('#connector').val();

    if( conector == "0" ){
        $(this).html(`${txtAnd}`);
        $('#connector').val('1');
    } else {
        $(this).html(`${txtOr}`);
        $('#connector').val('0');
    };
});

$('#selectCampo').on('change', function(){
    let idCampo = this.value;

    for (let i = 0; i < gon.tCamposLays.length; i++){
        let campo = gon.tCamposLays[i].IdCampo

        if ( idCampo == campo ){
            let pos =  gon.tCamposLays[i].Posicion,
                long =  gon.tCamposLays[i].Longitud;

            $('#posEdit').val(`${pos}`);
            $('#lengEdit').val(`${long}`);
        }
    }

});

$('#selectOperator').on('change', function(){
    let val = this.value;

    if ( val == "8" ){
        $('.valDos').css('display','block');
    } else {
        $('.valDos').css('display','none');
        $('#valorDos').val('');
    }

});
