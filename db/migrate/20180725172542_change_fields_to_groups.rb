class ChangeFieldsToGroups < ActiveRecord::Migration[5.0]

  def up
    change_table :groups do |t|
      t.change :detail, :string
      t.change :reference, :string
      t.change :area, :string
      t.change :hist_alert, :string
      t.change :hist_trans, :string
    end
  end

  def down
    change_table :groups do |t|
      t.change :detail, :integer
      t.change :reference, :integer
      t.change :area, :integer
      t.change :hist_alert, :integer
      t.change :hist_trans, :integer
    end
  end

end
